<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\BggController;
use App\Http\Controllers\API\AccountController;
use App\Http\Controllers\API\GiftingController;
use App\Http\Controllers\API\DrawController;
use App\Http\Controllers\API\PhasesController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:sanctum')->group(function() {
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('draw_date', [DrawController::class, 'getDate']);
    Route::get('confirm_date', [DrawController::class, 'getConfirmationDate']);
    
    Route::prefix('account')->name('account.')->group(function() {
        Route::get('receiver', [GiftingController::class, 'getReceiver']);
        Route::get('my_sender', [GiftingController::class, 'getMySender']);
        Route::get('confirmed', [AccountController::class, 'getStatus']);
        Route::post('confirm', [AccountController::class, 'confirm']);
        Route::post('sent', [GiftingController::class, 'sent']);
    });
});

Route::get('get-phase', [PhasesController::class, 'get']);
Route::get('bgg', [BggController::class, 'check']);
Route::get('/user', [AuthController::class, 'checkAuth']);
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);