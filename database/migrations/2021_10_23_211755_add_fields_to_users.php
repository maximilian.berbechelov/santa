<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {            
            $table->string('phone')->after('email');
            $table->string('bgg')->nullable()->after('phone');
            $table->string('courier')->after('bgg');
            $table->string('city')->after('courier');
            $table->string('type')->after('city');
            $table->string('address')->nullable()->after('type');
            $table->string('office')->nullable()->after('address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->dropColumn('bgg');
            $table->dropColumn('courier');
            $table->dropColumn('city');
            $table->dropColumn('type');
            $table->dropColumn('address');
            $table->dropColumn('office');
        });
    }
}
