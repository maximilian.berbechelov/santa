<?php

return [
    'confirmation_starts' => env('CONFIRMATION_STARTS', ''),
    'draw_starts' => env('DRAW_STARTS', ''),
    'sms_from' => env('SMS_FROM', 'SecretSanta'),
];