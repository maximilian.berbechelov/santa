module.exports = {
    purge: {
        enabled: false,
    },
    //   './resources/**/*.blade.php',
    //   './resources/**/*.js',
    //   './resources/**/*.vue',
    darkMode: false, // or 'media' or 'class'
    theme: {
        fontFamily: {
            'mont': 'Montserrat',
            'open': 'Open Sans',
            'roboto': 'Roboto',
            'lato': 'Lato',
        },
        extend: {
            colors: {
                'xmas-red': '#b71a3b',
                'xmas-dark-red': '#7e0f12',
                'xmas-brown': '#bc6d4c',
                'xmas-olive': '#6a7045',
                // 'xmas-gray': '#313c33',
                'xmas-gray': '#364739',
                'xmas-blue': '#072b54'
            },
            width: {
                '124': '60rem',
            },
            height: {
                '100': '36rem'
            },
            minHeight: {
                '100': '36rem'
            },
        },
    },
    variants: {
        extend: {
            opacity: ['disabled'],
            backgroundColor: ['disabled'],
            borderColor: ['disabled'],
            textColor: ['disabled'],
        },
    },
    plugins: [
        require('@tailwindcss/forms')
    ],
}
