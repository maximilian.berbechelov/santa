@component('mail::message')
# Здравей, {{ $user->name }},

Регистрацията за инциативата "Таен Дядо Коледа 2022" на "Шоуто на Максимилиан Бербечелов" и Collective View беше успешна! 
Можеш да влезеш използвайки следния бутон:

@component('mail::button', ['url' => env('APP_URL') . "/tokenLogin/" . $user->hash ])
Вход
@endcomponent

Можеш да запазиш следния линк, чрез който можеш да влизаш автоматично: {{ env('APP_URL') }}/tokenLogin/{{ $user->hash }}<br />&nbsp;
  
Ако все пак решиш да влезеш през страницата <a href='{{ env("APP_URL") }}/login'>за вход</a> - това е идентификационния ти хеш:

@component('mail::panel')
{{ $user->hash }}
@endcomponent

Благодарим,<br>
и Весели Празници!
@endcomponent