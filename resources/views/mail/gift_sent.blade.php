@component('mail::message')
# Здравей, {{ $receiver->name }},

Пишем ти, за да те уведомим, че твоят Таен Дядо Коледа току що маркира подаръкът ти като изпратен!

Ние сме не по-малко развълнувани и нямаме търпение да разберем какво ще получиш!

Очаквай го тук:

@component('mail::panel')
@if($receiver->courier == 3)
Студиото ни на "Златовръх 51"!
@else
{{ $receiver->courier_name }}, {{ $receiver->type_of_delivery }} {{ $receiver->place_of_delivery }}
@endif
@endcomponent

@if($receiver->courier == 3 || $receiver->big_opening == 'live')
Очакваме те в Студиото на 11 Декември след 11:00!
@elseif($receiver->big_opening == 'send_video')
Очакваме да ни изпратиш видеото с разопаковането си на santa@maxshow.bg!
@else
Надяваме се да сме ти донесли само положителни емоции!
@endif

Благодарим ти, че беше част от нашата инициатива!<br>
Весели Празници!
@endcomponent