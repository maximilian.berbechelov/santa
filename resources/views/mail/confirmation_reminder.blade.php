@component('mail::message')
# Здравей, {{ $user->name }},

Тайният ни Дядо Коледа минава на следваща фаза! Имаш три дни да влезеш в платформата и да потвърдиш участието си!
Както обикновено, можеш да използваш следния линк, за да влезеш автоматично:

@component('mail::button', ['url' => env('APP_URL') . "/tokenLogin/" . $user->hash ])
Вход
@endcomponent

Ако случайно си забравил идентификационния си хеш (а ние знаем, че си) ето го тук:

@component('mail::panel')
{{ $user->hash }}
@endcomponent

Правилата можеш да си припомниш <a href='https://youtu.be/XOWgOHpz0eU' target="_blank">тук</a>, а полезни впечатления от миналогодишното издание може да намериш <a href='https://youtu.be/H7w8srZuWS0' target="_blank">тук</a>.

Благодарим,<br>
и Весели Празници!
@endcomponent