import { createStore } from 'vuex'

import axios from 'axios'
import PhaseModule from './Modules/Phase'
import MainModule from './Modules/Main'
import RegisterModule from './Modules/Register'
import AuthModule from './Modules/Auth'

export const store = createStore({
    modules: {
        main    : MainModule,
        register: RegisterModule,
        auth: AuthModule,
        phase: PhaseModule,
    }
})