import Axios from "axios";

const main = {
    namespaced: true,
    state: () => ({
        authUser: null,
    }),
    getters: {
        authUser: state => state.authUser,
    },
    mutations: {
        authUser: (state, value) => state.authUser = value,
    },
    actions: {
        getAuthUser: ({ commit }) => {
            return axios.get('/api/user').then((r) => {
                commit('authUser', r.data)
            })
        }
    }
}

export default main;