const main = {
    namespaced: true,
    state: () => ({
        loading: true,
        registrationsOpen: true,
        authUser: null,
    }),
    getters: {
        registrationsOpen: state => state.registrationsOpen,
    },
    mutations: {
        startLoading: (state) => state.loading = true,
        stopLoading: (state) => state.loading = false,
        setRegistrationsOpen: (state, result) => state.items = result,
    },
    actions: {
        startLoading: ({ commit }) => commit('startLoading'),
        stopLoadingDelay: ({ commit }) => setTimeout(() => { commit('stopLoading') }, 500),
        stopLoading: ({ commit }) => commit('stopLoading'),
        async checkIfRegistrationsAreOpen({ commit }) {
            let response = await axios.get('api/open');
            commit('setRegistrationsOpen', response.data)
        }
    }
}

export default main;