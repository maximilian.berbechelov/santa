const phase = {
    namespaced: true,
    state: () => ({
        current: 'application',
    }),
    getters: {
        currentPhase: state => state.current,
        isApplication: state => state.current == 'application',
        isConfirmation: state => state.current == 'confirmation',
        isGifting: state => state.current == 'gifting',
        isEnded: state => state.current == 'ended',
    },
    mutations: {
        setCurrentPhase: (state, result) => state.current = result,
    },
    actions: {
        async getCurrentPhase({ commit }) {
            let response = await axios.get('api/get-phase');
            commit('setCurrentPhase', response.data)
        },
    }
}

export default phase;