const register = {
    namespaced: true,
    state() {
        return {
            step: 1,
            registrationData: {
                name    : '',
                email   : '',
                bgg     : '',
                // courier : {id: 1, name: 'Еконт Експрес'},
                courier : null,
                // type    : { id: 2, name: 'До адрес' },
                type    : null,
                office  : '',
                address : '',
                city    : '',
                phone   : '',
            }
        }
    },
    getters: {
        currentStep: state => state.step,
        isHome: state => state.registrationData.type.id == 2,
        isOffice: state => state.registrationData.type.id == 1,
        isForStudio: state => state.registrationData.courier.id == 3,
        courierName: state => state.registrationData.courier.name
    },
    mutations: {
        stepForward: (state) => state.step++,
        stepBackward: (state) => state.step--
    },
    actions: {
        advance({ commit }) {
            commit('main/startLoading', null, { root: true })

            setTimeout(() => {
                commit('stepForward');
                commit('main/stopLoading', null, { root: true })
            }, 500);
        },
        regres({ commit }) {
            
            setTimeout(() => {
                commit('stepBackward');
;            }, 500);
        }
    }
}

export default register;