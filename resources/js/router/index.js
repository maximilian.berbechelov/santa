import { createWebHistory, createRouter } from "vue-router"
import Register from "@/pages/Register"
import Home from "@/pages/Home"
import Login from "@/pages/Login"
import Dashboard from "@/pages/Dashboard"
import Terms from "@/pages/static/Terms"
import Unboxing from "@/pages/static/Unboxing"
import PageNotFound from "@/pages/PageNotFound"
import TokenLogin from "@/pages/TokenLogin"
import { store } from "@/store"

const appTitle = 'Secret Santa 2022'

const routes = [
    {
        path     : "/",
        component: Home,
        name     : 'home',
        meta     : {
            title: 'Начало'
        }
    },
    {
        path     : "/register",
        component: Register,
        name     : "register",
        meta     : {
            title: 'Регистрация'
        }
    },
    {
        path     : '/tokenlogin/:token',
        component: TokenLogin,
        name     : "tokenLogin",
        props    : true,
        meta     : {
            title: 'Вход'
        }
    },
    {
        path     : "/login",
        component: Login,
        name     : "login",
        meta     : {
            title: 'Вход'
        }
    },
    {
        path     : '/terms',
        component: Terms,
        name     : "terms",
        meta     : {
            title: 'Условия за участие',
        }
    },
    {
        path     : '/unboxing',
        component: Unboxing,
        name     : "unboxing",
        meta     : {
            title: "#ГолямотоРазопаковане"
        }
    },
    {
        path     : "/dashboard",
        component: Dashboard,
        name     : "dashboard",
        meta     : {
            title       : 'Дашборд',
            requiresAuth: true
        }
    },
    {
        path     : "/:catchAll(.*)*",
        component: PageNotFound,
        name     : '404',
        meta     : {
            title: 'О-оу!'
        }
    }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
    const reqAuth = to.matched.some((record) => record.meta.requiresAuth);

    if (reqAuth) {
        store.dispatch("auth/getAuthUser").then(() => {
            if (! store.getters["auth/authUser"]) {
                next({ name: 'home' });
            } else {
                next();
            }
        });
    } else {
        next(); 
    }
});

router.beforeEach((toRoute, fromRoute, next) => {
    let routeName = toRoute.meta && toRoute.meta.title ? toRoute.meta.title : 'Home';
    window.document.title = routeName + ': ' + appTitle;
    next();
})

export default router;