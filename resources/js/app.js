require('./bootstrap');

import { createApp } from "vue";
import router from './router'
import { store } from './store'
import App from './App.vue'

axios.defaults.withCredentials = true
      
const app = createApp({
    data() {
        return {
        }
    },
    components: {
        App,
    },
}).use(store).use(router).mount('#app')