<?php

namespace App\Services\SMS;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;

abstract class SMS {

    protected function http(): PendingRequest
    {
        return Http::withHeaders([
            'Authorization' => implode(' ', ['App', $this->apiKey()]),
        ]);
    }

    protected function apiKey(): String
    {
        return config('services.sms.key');
    }

    protected function baseUrl(): String
    {
        return config('services.sms.endpoint');
    }

    public static function prepareForInternational(String $phone): String
    {
        return '359' . ltrim($phone, "0");
    }

    abstract protected function endpoint(Array|Null $params): String;
}