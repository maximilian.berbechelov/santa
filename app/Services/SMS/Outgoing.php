<?php

namespace App\Services\SMS;

use Illuminate\Http\Client\Response;

class Outgoing extends SMS {

    public function send(Array|Null $params): Response
    {
        return $this->http()->post($this->endpoint(['sms', 2, 'text', 'advanced']), $params);
    }

    protected function endpoint(Array|Null $params): String
    {
        return implode('/', [
            $this->baseUrl(),
            implode('/', $params),
        ]);
    }

}