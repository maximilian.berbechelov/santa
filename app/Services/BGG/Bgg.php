<?php

namespace App\Services\BGG;

use Illuminate\Support\Facades\Http;

class Bgg {
    
    public function checkForExistingUser($user)
    {
        $xml = file_get_contents(implode('/', [$this->endpoint(), 'user']) . '?name=' . $user);
        return $this->xmlToArray($xml)['@attributes']['id'] ? true : false;
    }

    private function xmlToArray($string)
    {
        $xml = simplexml_load_string($string, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        return json_decode($json, TRUE);
    }
    

    private function endpoint()
    {
        return config('services.bgg.endpoint');
    }

}