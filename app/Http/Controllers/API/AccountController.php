<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ConfirmationRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AccountController extends Controller
{
    public function getStatus()
    {
        return auth()->user()->confirmed;
    }

    public function confirm(ConfirmationRequest $request)
    {
        $user = User::find(Auth::user()->id);        

        $user->bio = $request->bio;
        $user->big_opening = $request->bigOpening['id'];
        $user->confirmed = true;

        $user->save();
        
        return true;
    }
}
