<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Mail\GiftSent;
use App\Models\Receiver;
use Illuminate\Support\Facades\Mail;
use App\Services\SMS\Outgoing;

class GiftingController extends Controller
{
    public function getReceiver()
    {
        return auth()->user()->receiver;
    }

    public function getMySender()
    {
        return auth()->user()->giver;
    }

    public function sent()
    {
        $user = auth()->user();
     
        Receiver::getByGiver($user)->update([
            'sent' => now()
        ]);

        Mail::to($user->receiver)->send(new GiftSent($user->receiver));

        $api = new Outgoing;
        
        $api->send([
            'messages' => [
                'destinations' => ['to' => Outgoing::prepareForInternational($user->receiver->phone)],
                'text'         => $user->receiver->name . ', твоят Таен Дядо Коледа току що маркира подаръкът ти като изпратен!',
                'from'         => config('santa.sms_from'),
            ]
        ]);

        return true;
    }
}
