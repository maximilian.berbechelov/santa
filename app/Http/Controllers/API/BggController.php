<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\BGG\Bgg;

class BggController extends Controller
{
    public function check(Request $request)
    {
        if(! $request[0]) {
            return false;
        }
        
        $api = new Bgg;
        return $api->checkForExistingUser($request[0]);
    }
}
