<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;

class PhasesController extends Controller
{
    public function get()
    {
        $dates = [
            'confirmation_starts' => Carbon::parse(config('santa.confirmation_starts'))->startOfDay(),
            'draw_starts' => Carbon::parse(config('santa.draw_starts'))->startOfDay(),
        ];

        if(now()->gt($dates['draw_starts'])) {
            return 'gifting';
        }

        if(now()->gt($dates['confirmation_starts'])) {
            return 'confirmation';
        }

        return 'application';
    }
}
