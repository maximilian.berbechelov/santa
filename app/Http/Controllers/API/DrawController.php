<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

class DrawController extends Controller
{
    public function getDate()
    {
        return [
            'date'          => env('DRAW_DATE_BG'),
            'remainingDays' => Carbon::parse(env('DRAW_DATE'))->diffInDays(now()),
        ];
    }

    public function getConfirmationDate()
    {
        return [
            'date'          => env('CONFIRM_STARTS_BG'),
            'remainingDays' => Carbon::parse(env('CONFIRMATION_STARTS'))->diffInDays(now()),
        ];
    }
}
