<?php

namespace App\Http\Controllers;

use App\Mail\WelcomeMail;
use App\Models\User;
use Illuminate\Http\Request;
use App\Services\BGG\Bgg;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;

class TestController extends Controller
{
    public function index() 
    {  
        $user = User::find(1);
        return (new WelcomeMail($user))->render();
        // Mail::to($user)->send(new WelcomeMail($user));
    }
}
