<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Mail\WelcomeMail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Carbon\Carbon;

class AuthController extends Controller
{
    public function register(RegisterRequest $request)
    {
        $user = User::firstOrCreate([
            'email'    => $request->email,
        ], [
            'name'     => $request->name,
            'phone'    => $request->phone,
            'bgg'      => $request->bgg,
            'courier'  => $request->courier['id'],
            'city'     => $request->city,
            'type'     => property_exists($request, 'type') ? $request->type['id'] : '',
            'hash'     => $this->generateLoginToken(),
            'address'  => $request->address ?? null,
            'office'   => $request->office ?? null,
            'password' => Hash::make($request->password)
        ]);

        Mail::to($user)->send(new WelcomeMail($user));

        return response()->json(['message' => 'Successfuly created user']);

    }

    public function login(Request $request)
    {
        $request->validate([ 'hash' => 'required' ]);
        if($user = User::whereHash($request->hash)->first())
            Auth::login($user);
        else
            return response([ 'error' => 'not_found' ]);   
    }

    public function logout(Request $request)
    {
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return response("OK", 200);
    }

    private function generateLoginToken()
    {
        $pool = 'abcdefghijklmnopqrstuvwxyz';
        $token = substr(str_shuffle(str_repeat($pool, 7)), 0, 7);

        if(User::whereHash($token)->first()) {
            $this->generateLoginToken();
        } else return $token;
    }

    public function getPhase()
    {
        $endingDate = Carbon::parse(env('DRAW_DATE'))->startOfDay();
        return ! $endingDate->isPast();
    }

    public function checkAuth()
    {
        return Auth::user();
    }
}
