<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required',
            'phone'    => 'nullable|digits:10',
            'email'    => 'required|email',
            'bgg'      => 'nullable',
            'courier'  => 'nullable',
            'city'     => 'nullable',
            'type'     => 'nullable',
            'address'  => 'required_if:delivery_type[id],2',
            'office'   => 'required_if:delivery_type[id],1',
        ];
    }
}
