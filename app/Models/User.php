<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'bgg',
        'courier',
        'city',
        'type',
        'hash',
        'sends_to',
        'address',
        'office',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $appends = [
        'bgg_full_url',
        'place_of_delivery',
        'courier_name',
        'type_of_delivery',
        'sent',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getReceiverAttribute()
    {
        if(Receiver::getByGiver($this))
            return self::find($this->decrypt(Receiver::getByGiver($this)->receiver_id));
        
        return null;
    }

    public function receiverRecord()
    {
        return $this->hasOne(Receiver::class);
    }

    public function getSentAttribute()
    {
        $receiver = Receiver::getByGiver($this);
        return $receiver ? $receiver->sent : false;
    }

    public function getGiverAttribute()
    {
        if($receiver = Receiver::getByReceiver($this))
            return self::find($receiver->user_id);
        
        return null;
    }

    public function decrypt(String $hash): String
    {
        return Crypt::decrypt($hash);
    }

    public function scopeWithoutReceivers(Builder $query): Builder
    {
        return $query->whereDoesntHave('receiverRecord');
    }

    public function scopeFree(Builder $query)
    {
        $receivers = array_map('Crypt::decrypt', Receiver::getAllReceiversAsArray());    
        return $query->whereNotIn('id', $receivers);
    }

    public function scopeConfirmed($query)
    {
        return $query->where('confirmed', 1);
    }

    public function scopeUnconfirmed($query)
    {
        return $query->where('confirmed', 0);
    }

    public function scopeNotSelf($query, $user)
    {
        return $query->where('id', '!=', $user->id);
    }

    public function getBggFullUrlAttribute()
    {
        return 'https://boardgamegeek.com/user/' . $this->bgg;
    }

    public function getCourierNameAttribute()
    {
        switch($this->courier) {
            case 1:
                return 'Еконт';
                break;
            case 2:
                return 'Спиди';
                break;
            case 3: 
                return 'Студиото ни';
                break;

        }
    }

    public function getPlaceOfDeliveryAttribute()
    {
        switch($this->type)
        {
            case 1: 
                return $this->office;
                break;
            case 2:
                return $this->address;
                break;
            default:
                return null;
                break;
        }
    }

    public function getTypeOfDeliveryAttribute()
    {
        switch ($this->type) {
            case 1:
                return 'офис';
                break;
            case 2:
                return 'адрес';
                break;
            default:
                return 'офис';
                break;
        }
    }

    public function appendReceiver(User $receiver)
    {
        Receiver::create([
            'user_id' => $this->id,
            'receiver_id' => Crypt::encrypt($receiver->id),
        ]);
    }
}
