<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Receiver extends Model
{
    use HasFactory;

    public $guarded  = ['id'];

    public $timestamps = false;

    protected $casts = [
        'sent' => 'datetime',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getAllReceiversAsArray()
    {
        return self::pluck('receiver_id')->filter()->toArray();
    }

    public static function getByReceiver(User $user)
    {
        return self::get()->filter(function(Receiver $receiver) use($user) {
            return Crypt::decrypt($receiver->receiver_id) == $user->id;
        })->first();
    }

    public static function getByGiver(User $user)
    {
        return self::whereUserId($user->id)->first();
    }
}
