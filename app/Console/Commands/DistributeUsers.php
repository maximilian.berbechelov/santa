<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;

class DistributeUsers extends Command
{
    protected $signature = 'users:distribute';

    protected $description = 'Distributes givers and receivers';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {        
        User::confirmed()->withoutReceivers()->each(function($user) {
            $receiver = User::notSelf($user)->confirmed()->free()->inRandomOrder()->first();
            $user->appendReceiver($receiver);
        });
    }
}
