<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\SMS\Outgoing;
use App\Models\User;

class SendReminderSms extends Command
{

    const FROM = 'SecretSanta';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends reminder SMS to people who have still not confirmed their accounts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $api = new Outgoing;

        User::unconfirmed()->each(function(User $user) use($api) {
            $response = $api->send([
                'messages' => [
                    'destinations' => ['to' => Outgoing::prepareForInternational($user->phone)],
                    'text'         => $this->generateText($user),
                    'from'         => self::FROM,
                ]
            ]);

            $this->info(json_encode($response->json()));
        });      

        return true;
    }

    protected function generateText(User $user): String
    {
        return 'Здравей, ' . $user->name . ', пишем ти от Secret Santa, за да ти напомним, че имаш малко време да потвърдиш участието си в инициативата! Можеш да направиш това на https://santa.maxshow.bg (за вход: ' . $user->hash . ') или като посетиш линка в мейла, който сме ти изпратили. Ако срещаш затруднения може да ни пишеш на santa@maxshow.bg.';
    }
}
