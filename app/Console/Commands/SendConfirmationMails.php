<?php

namespace App\Console\Commands;

use App\Mail\ConfirmationReminder;
use Illuminate\Console\Command;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class SendConfirmationMails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminders:confirmation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends reminders for confirming the users profiles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::whereConfirmed(false)->get();

        $users->each(function($user) {
            Mail::to($user)->send(new ConfirmationReminder($user));
        });
    }
}
